# AV6 - Labirinto 

Entrega da 6ª avaliação do ano de 2023!

## Descrição

O jogo consiste em um labirinto construído em python e que tem como intuito fazer com que o jogador chegue ao final do percurso e saía do desafio. Ademais, o projeto conta com uma página web para possibilitar a visualização dos resultados.

## Construção
O labirinto foi desenhado utilizando uma abordagem diferenciada, visto que com o auxilio de uma lista o desafio é desenhado através de strings. Como exemplificado abaixo:
 ```
mapa = [
    "WWWWWWWWWWWWWWWWWWWW",
    "W                  W",
    "W         WWWWWW   W",
    "W   WWWW       W   W",
    "W   W        WWWW  W",
    "W WWW  WWWW        W",
    "W   W     W W      W",
    "W   W     W   WWW  W",
    "W   WWW WWW   W W  W",
    "W     W   W   W W  W",
    "WWW   W   WWWWW W  W",
    "W W      WW        W",
    "W W   WWWW   WWW   W",
    "W     W        W   W",
    "WWWWWWWWWWWEWWWWWWWW",
]

```
Já o esquema da criação do labirinto consiste me uma série de comandos simples e muito intuitivos. Veja a seguir:

```
for row in mapa: #percorre cada linha da lista mapa
    for col in row: #percorre cada coluna de cada linha
        if col == "W": #veifica se existe uma parede no local
            paredes_group.add(Obstaculo(eixoX, eixoY, 30, 30)) #Adiciona o obstaculo no grupo de obstaculos com as dimensões indicadas

        if col == "E": #Verifica se existe a saída em determinado local
            end_rect = pygame.Rect(eixoX, eixoY, 30, 30) #Define as dimensões
            finalX = eixoX
            finalY = eixoY
        eixoX += 30
    eixoY += 30
    eixoX = 0

```
O jogo também possui uma página HTML que mostra o nome do jogador (que é informado no início da partida) e o número de movimentos que ele realizou para solucionar o desafio.

## Embelezamento 

O embelezamento do site foi construído fazendo uso da linguagem de front-end CSS, já as imagens foram manipuladas utilizando um site para remover o fundo (https://www.remove.bg/pt-br/upload) e outro para ajustar o tamanho (https://imageresizer.com/pt).

Segue imagens do labirinto e da página web:

<img src="image.png">
<img src="paginaweb.png">
